puts "Welcome to this Ruby code that will give you some statistics on a text file"

print  "Give me the absolute path of the .txt file that need to be analyzed: "

filepath = gets.chomp

nbline = 0
nbwords = 0
nbletters = 0
nbchar = 0

# TO DO: Add error management if file path isn't correct or file extension is wrong
file = File.open(filepath)

#Get file content as an array of line
linearray = file.readlines.map(&:chomp)

# Get numbers of line
nbline = linearray.size

# Get the number of words
# TO DO: This includes numbers => to be improved in a later version to only include words
linearray.each { |line|
    word = line.split(' ')
    puts "words: #{word}"
    nbwords += word.size
}

# Get the number of chars (including letters, numbers, spaces, special character
text = linearray.join 
puts "full text: #{text}"
nbchar = text.length 
puts "text gsub: #{text.gsub(/\s+/, "")}"
nbletters = text.gsub(/\s+/, "").length


puts "This file has #{nbline} lines" 
puts "This file has #{nbwords} words " 
puts "This file has #{nbletters} letters " 
puts "This file has #{nbchar} characters (letters, numbers, spaces, special characters)" 

# Advanced stats on words with 1 letter, 2 letters, ...
# Considering that the longest word is 40 characters
wordbysize = Array.new(40) 
wordbysize.map! {|item| item=0}

linearray.each { |line|
    word = line.split(' ')
    word.each { |singleword|
        sizeofcurrentword = singleword.size
        wordbysize[sizeofcurrentword] += 1
    }
    puts "words by size: #{word}"
    nbwords += word.size
}

index = 0
wordbysize.each {|item| 
    if item != 0
        tobe=''
        word="word"
        letter = "letter"
        if index > 1
            letter = "letters"
        end
        if item == 1 
            tobe= "is" 
        else
            tobe= "are"
            word= "words"
        end
        puts "There #{tobe} #{item} #{word} with #{index} #{letter} in the file"
    end
    index += 1
}
